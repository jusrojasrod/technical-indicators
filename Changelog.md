# Changelog

## 0.0.1 - 2023-06-29
---
### Added
- The next indicators functions:

    - add_SuperPassbandFilter.
    - add_PricingRF.
    - add_PercentagePriceOscillator.

- Download from EODHD function.

### Changed

- Notebook named 'untitled' was rename to 'indicatorsTest.ipynb'
