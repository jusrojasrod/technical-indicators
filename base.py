from datetime import date
import requests
import pandas as pd

import config as cfg

def downloadData(symbol, years_back, api_key=cfg.api_key,
                 end_date=date.today()):
    """
    Data downloaded from EODHD API.
    Parameters
    ----------
    symbol : str
        Ticker symbol.
    years_back : int
        Number of years back to pull the data.
    api_key : str
        API access key.
    end_date : datetime.date, str
        End date to pull out data.

    Return
    ------
    data : pandas.core.frame.DataFrame
        Ticker OHLC data.

    """

    start_date = date(end_date.year - years_back, end_date.month, end_date.day)
    end_date = end_date.strftime("%Y-%m-%d")
    start_date = start_date.strftime("%Y-%m-%d")

    url = f"https://eodhistoricaldata.com/api/eod/{symbol}?from={start_date}&to={end_date}&period=d&fmt=json&api_token={api_key}"

    try:
        data = requests.get(url, timeout=20)
        data = data.json()
        data = pd.DataFrame(data)
        data.set_index(['date'], inplace=True)

        return data

    except:
        print(f"{symbol} not found!!!", data)
        return pd.DataFrame()