import numpy as np
import pandas as pd


def add_SuperPassbandFilter(data, column_name="Close", fastLength=40, slowLength=60):
    """
    Function that calculates the Ehlers Super Passband Filter.

    Parameters
    ----------
    df : pandas.core.frame.DataFrame
        Dataframe containing the prices data.
    column_name : str, optional
        default value = 'Close'. It gets the close price from the dataframe.
    fastLength = int, optional
        Default value = 40.
    slowLength = int, optional
        Default value = 40.

    Returns
    -------
    espf_list : numpy.ndarray
    rms : pandas.core.series.Series
        An array of the same len as 'espf_list', containing the Root
        Mean Square.

    """
    df = data.copy()

    src = df[column_name].to_list()
    espf_list = np.zeros(df[column_name].count())
    square_espf = []
    rms_list = []
    neg_rms_list = []

    a1 = 5 / fastLength
    a2 = 5 / slowLength

    for i in range(df[column_name].count()):

        prevSrc = np.where(i >= 1, src[i-1], 0)
        prevEspf1 = np.where(i >= 1, espf_list[i-1], 0)
        prevEspf2 = np.where(i >= 2, espf_list[i-2], 0)

        espf_list[i] = (((a1 - a2) * src[i])
                        + ((a2 * (1 - a1) - a1 * (1 - a2)) * prevSrc)
                        + (((1 - a1) + (1 - a2))*prevEspf1)
                        - ((1 - a1) * (1 - a2) * prevEspf2))

    square_espf = pd.Series(np.power(espf_list, 2))
    rms = np.sqrt(square_espf.rolling(50).mean())

    df["espf"] = espf_list
    df["RMS"] = rms.values
    df["-RMS"] = -rms.values

    df.dropna(inplace=True)

    return df


def add_PricingRF(data, column_name="Close", length=14, signalType="SMA",
                  signalLength=9):
    """
    Input:
        * dataFrame shape data
        * length
        * signalType: It could be SMA or EMA
        * signalLength
    Returns:
        trix, signal, hist. numpy array type

    Note: plot trix and signal
    """
    
    ema1 = data[column_name].ewm(span=length, adjust=False).mean()
    ema2 = ema1.ewm(span=length, adjust=False).mean()
    ema3 = ema2.ewm(span=length, adjust=False).mean()  # returns a serie

    ema3_prev = ema3.shift(1)

    trix = (ema3 / ema3_prev) - 1  # returns a serie data type


    # Second plot (signal)
    trix_df = pd.DataFrame(trix, columns=[column_name])
    if signalType == "SMA":
        # compute the SMA in a time frame equal to signalLength
        signal = trix_df[column_name].rolling(window=signalLength).mean().dropna()
    else:
        # ema camputation
        signal = trix_df.ewm(span=signalLength, adjust=False).mean()

    data['PricingRF_fast'] = trix * 100
    data['PricingRF_slow'] = signal * 100
    
    return data
